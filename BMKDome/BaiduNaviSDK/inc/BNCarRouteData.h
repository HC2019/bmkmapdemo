//
//  BNCarRouteData.h
//  BNDriverPageKit
//
//  Created by linbiao on 2019/5/16.
//  Copyright © 2019年 Chen,Xintao. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BNCarRouteNode;
@class BNYellowTipsInfoGroup;

NS_ASSUME_NONNULL_BEGIN

/*!
 @class
 @abstract 多路线总信息
 */
@interface BNCarRouteModel : NSObject

/// 多路线，NSMutableArray的内容为BNCarOneRouteModel
@property (nonatomic, strong) NSMutableArray *carRoutes;

@end


/*!
 @class
 @abstract 单独的一条路线信息
 */
@interface BNCarOneRouteModel : NSObject

/// 默认线路小面板显示内容
@property (nonatomic, strong) NSString *routeLabelName;

/// 总路程（米)
@property (nonatomic, assign) NSInteger distance;

/// 预计的行驶时间 ,分
@property (nonatomic, assign) NSInteger time;

/// 红绿灯个数
@property (nonatomic, assign) NSInteger lightNum;

/// 高速收费
@property (nonatomic, assign) NSInteger toll;

/// 路段列表,NSMutableArray的内容为BNCarRouteStepModel
@property (nonatomic, strong) NSMutableArray *stepList;

/// 起点
@property (nonatomic, strong) BNCarRouteNode *startPoint;

/// 途经点 BNCarRouteNode
@property (nonatomic, strong) NSArray * viaPointArray;

/// 终点
@property (nonatomic, strong) BNCarRouteNode *endPoint;

/// 小黄条集合，可能为空
@property (nonatomic, strong) BNYellowTipsInfoGroup *tipsInfoGroup;

@end


/*!
 @class
 @abstract 路段信息
 */
@interface BNCarRouteStepModel : NSObject

/// 以米为单位的路线长度
@property (nonatomic, assign) NSInteger distance;

/// 以秒为单位的路线花费时间
@property (nonatomic, assign) NSInteger time;

/// 路段的提示语，按照html格式组织
@property (nonatomic, copy) NSString *linkFormatTip;

/// 该路段导航转向类型
@property (nonatomic, assign) NSInteger turn;

@end


/*!
 @class
 @abstract 节点
 */
@interface BNCarRouteNode : NSObject

/// 节点名称
@property (nonatomic, copy) NSString *name;

@end


/*!
 @class
 @abstract 小黄条集合
 */
@interface BNYellowTipsInfoGroup : NSObject

/// 小黄条集合，NSMutableArray的内容为BNYellowTipsInfo
@property (nonatomic, strong) NSMutableArray *tipsInfoArray;

@end


/*!
 @class
 @abstract 单个小黄条数据结构
 */
@interface BNYellowTipsInfo : NSObject

/// 小黄条标题
@property (nonatomic, copy) NSString *title;

@end


/*!
 @class
 @abstract 工具类
 */
@interface BNCarRouteDataUtil : NSObject

+ (NSString *)typeIconNameOfEnum:(NSInteger)type;

@end

NS_ASSUME_NONNULL_END
