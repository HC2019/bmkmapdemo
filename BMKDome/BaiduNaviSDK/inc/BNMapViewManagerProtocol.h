//
//  BNMapViewInterface.h
//  
//
//  Created by chenxintao on 2017/11/21.
//

#ifndef BNMapViewInterface_h
#define BNMapViewInterface_h
#import <Foundation/Foundation.h>

@protocol BNMapViewManagerDelegate;

@interface BNMapViewStatus : NSObject

//地图中心
@property (strong, nonatomic) BNPosition* center;

//比例尺
@property (assign, nonatomic) double level;

//旋转角
@property (assign, nonatomic) double rotation;

//仰角
@property (assign, nonatomic) double overlooking;

//中心偏移值
@property (assign, nonatomic) CGPoint offset;

@end

//地图的view，其他的接口待定
@protocol BNMapViewManagerProtocol

@property (nonatomic, weak) id<BNMapViewManagerDelegate>delegate;

/**
 获取底图对象
 
 @return 底图view
 */
- (UIView*)getMapView;

/**
 获取底图上的logo，默认在左下角
 
 @return 底图上的logo
 */
- (UIView*)logoView;

/**
 设置底图的offset

 @param offset 底图的offset，x为负数，往左移动，为正数，往右移；y为正数，往上移，为负数，往下移
 */
- (void)setMapViewOffset:(CGPoint)offset animated:(BOOL)animated;

/**
 设置地图的状态
 
 @param mapViewStatus 地图状态
 */
- (void)setMapViewStatus:(BNMapViewStatus*)mapViewStatus animated:(BOOL)animated;

/**
 获取地图的状态
 
 @return 地图状态
 */
- (BNMapViewStatus*)getMapViewStatus;

/// 放大地图, 返回YES表示最大比例尺，返回NO表示不是最大比例尺
-(BOOL)zoomIn;

/// 缩小地图，返回YES表示最小比例尺，返回NO表示不是最小比例尺
-(BOOL)zoomOut;

@end

@protocol BNMapViewManagerDelegate <NSObject>

/**
 地图缩放级别发生变化回调
 
 @param level 缩放级别
 */
- (void)onHandleMapLevelChange:(NSInteger)level;

@end

#endif /* BNMapViewInterface_h */
