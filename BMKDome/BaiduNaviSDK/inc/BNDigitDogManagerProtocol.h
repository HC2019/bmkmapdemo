//
//  BNDigitDogManagerProtocol.h
//  NaviBaseSDK
//
//  Created by Chen,Xintao on 2019/5/14.
//  Copyright © 2019 Chen,Xintao. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BNDigitDogManagerDelegate;

@protocol BNDigitDogManagerProtocol <NSObject>

@property (weak, nonatomic) id<BNDigitDogManagerDelegate> delegate;

/**
 开始电子狗
 
 @return 是否成功
 */
- (BOOL)startDigitDog;

/**
 结束电子狗
 
 @return 是否成功
 */
- (BOOL)stopDigitDog;

@end

@protocol BNDigitDogManagerDelegate <NSObject>

/**
 调用startDigitDog定位成功后开始电子狗
 */
- (void)onStartDigitDog;


/**
 定位错误
 */
- (void)onLocationError;


/**
 定位关闭或者用户禁止App定位权限
 */
- (void)onLocationClose;


@end

NS_ASSUME_NONNULL_END
