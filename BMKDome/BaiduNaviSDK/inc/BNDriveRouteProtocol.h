//
//  BNDriveRouteProtocol.h
//  BNDriverPageKit
//
//  Created by linbiao on 2019/5/8.
//  Copyright © 2019年 Chen,Xintao. All rights reserved.
//

#ifndef BNDriveRouteProtocol_h
#define BNDriveRouteProtocol_h

typedef struct BNMargin {
    CGFloat top, left, bottom, right;
} BNMargin;

#import <Foundation/Foundation.h>

@class BNCarRouteModel;
@protocol BNDriveRouteManagerDelegate;

@protocol BNDriveRouteProtocol <NSObject>

/**
 路线结果页面相关事件回调的delegate
 */
@property (nonatomic, weak) id<BNDriveRouteManagerDelegate> delegate;

/**
 全览路线
 
 @param margin 需要显示的路线范围的margin
 @param animated 全览是否需要动画
 */
- (void)showRouteViewAll:(BNMargin)margin animated:(BOOL)animated;

/**
 选中路线，调用该接口后，同时会高亮该路线
 
 @param routeIndex 路线序号（从0开始）
 */
- (void)selectRouteAtIndex:(NSUInteger)routeIndex;

/**
 驾车路线页即将显示，在viewWillAppear中调用
 
 @param parentView mapView的父view
 */
- (void)viewWillAppear:(UIView*)parentView;

/**
 驾车路线页即将消失，在viewWillDisAppear中调用
 
 @param parentView mapView的父view
 */
- (void)viewWillDisAppear:(UIView*)parentView;

/*
 打开定位，驾车页算路成功后需要打开定位才能显示车标
 */
- (void)startUpdateLocation;

/*
 关闭定位
 */
- (void)stopUpdateLocation;

/*
 获取当前路线信息
 
 @return 当前路线信息
 */
- (BNCarRouteModel *)getCurrentCarRouteData;

/**
 销毁BNDriveRouteManager相关资源
 */
- (void)destory;

@end

@protocol BNDriveRouteManagerDelegate <NSObject>

/**
 用户在地图上点击了某条路线（如果需要高亮该路线，要调用selectRouteAtIndex:接口）
 
 @param routeIndex 路线序号（从0开始）
 */
- (void)onHandleTouchRouteAtIndex:(NSUInteger)routeIndex;

/**
 回调车点
 
 @param positionInfo 车点信息
 */
- (void)onHandleUpdateCurrentCarPositionInfo:(NSDictionary *)positionInfo;

@end

#endif /* BNDriveRouteProtocol_h */
