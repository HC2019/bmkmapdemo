//
//  AppDelegate.swift
//  BMKDome
//
//  Created by Hanchao on 2019/10/17.
//  Copyright © 2019 hmcx. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, BMKLocationAuthDelegate, BNNaviSoundDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow.init()
        window?.frame = UIScreen.main.bounds
        window?.makeKeyAndVisible()
        window?.rootViewController = UINavigationController.init(rootViewController: ViewController())
                
        hc_createBauduMap()

        return true
    }
    
    
    //MARK: 初始化百度地图
    func hc_createBauduMap() {
        
        // 初始化定位SDK
        BMKLocationAuth.sharedInstance()?.checkPermision(withKey: BDMAPKey, authDelegate: self)

        // 要使用百度地图，请先启动BaiduMapManager
        let mapManager = BMKMapManager()
        // 如果要关注网络及授权验证事件，请设定generalDelegate参数
        let ret = mapManager.start(BDMAPKey, generalDelegate: nil)
        if !ret {
            print("manager start failed!");
        }
       /**
        全局设置地图SDK与开发者交互时的坐标类型。不调用此方法时，
    
        设置此坐标类型意味着2个方面的约定：
        1. 地图SDK认为开发者传入的所有坐标均为此类型；
        2. 所有地图SDK返回给开发者的坐标均为此类型；

        地图SDK默认使用BD09LL（BMK_COORDTYPE_BD09LL）坐标。
        如需使用GCJ02坐标，传入参数值为BMK_COORDTYPE_COMMON即可。ß
        本方法不支持传入WGS84（BMK_COORDTYPE_GPS）坐标。
    
        @param coorType 地图SDK全局使用的坐标类型
        @return 设置成功返回YES，设置失败返回False
        */
                
        BNaviService.getInstance()?.initNaviService(nil, success: {
            BNaviService.getInstance()?.authorizeNaviAppKey(BDMAPKey, completion: { (suc) in
                print("authorizeNaviAppKey ret = \(suc)")
            })
            
            BNaviService.getInstance()?.authorizeTTSAppId(BDTTSAPPID, apiKey: BDTTSAPPKEY, secretKey: BDTTSSECRETKEY, completion: { (suc) in
                print("authorizeTTS ret = \(suc)")
            })
            
            BNaviService.getInstance()?.soundManager()?.setSoundDelegate(self)
        }, fail: {
            print("initNaviSDK fail")
        })
        
        
    }    
    
    func onPlayTTS(_ text: String!) {
        
    }
    
    func onPlay(_ type: BNVoiceSoundType, filePath: String!) {
        
    }
    
    func onTTSAuthorized(_ success: Bool) {
        
    }
    
    

    // MARK: UISceneSession Lifecycle

//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }


}

