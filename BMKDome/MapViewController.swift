//
//  MapViewController.swift
//  BMKDome
//
//  Created by Hanchao on 2019/10/17.
//  Copyright © 2019 hmcx. All rights reserved.
//

import UIKit

class MapViewController: UIViewController {
    
    private var mapView             : BMKMapView!
    private var userLocation        : BMKUserLocation! = BMKUserLocation()
    private var startAnnotation     : BMKPointAnnotation?
    private var endAnnotation       : BMKPointAnnotation?
    private var annotationView      : BMKAnnotationView!
    private var customUserLocationView  : BMKAnnotationView!

    var dataArray: NSMutableArray = NSMutableArray()
    
    
    deinit {
        print("释放了")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.title = "地图展示"
        
        hc_createMapView()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+3) { [weak self] in
            self?.setupDefaultData()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mapView.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mapView.viewWillDisappear()
    }

    //MARK:Lazy loading
    lazy var locationManager: BMKLocationManager = {
        //初始化BMKLocationManager的实例
        let manager = BMKLocationManager()
        //设置定位管理类实例的代理
        manager.delegate = self
        //设定定位坐标系类型，默认为 BMKLocationCoordinateTypeGCJ02
        manager.coordinateType = BMKLocationCoordinateType.BMK09LL
        //设定定位精度，默认为 kCLLocationAccuracyBest
        manager.desiredAccuracy = kCLLocationAccuracyBest
//        设定定位类型，默认为 CLActivityTypeAutomotiveNavigation
        manager.activityType = CLActivityType.automotiveNavigation
        //指定定位是否会被系统自动暂停，默认为NO
        manager.pausesLocationUpdatesAutomatically = false
        /**
         是否允许后台定位，默认为NO。只在iOS 9.0及之后起作用。
         设置为YES的时候必须保证 Background Modes 中的 Location updates 处于选中状态，否则会抛出异常。
         由于iOS系统限制，需要在定位未开始之前或定位停止之后，修改该属性的值才会有效果。
         */
        manager.allowsBackgroundLocationUpdates = true
        /**
         指定单次定位超时时间,默认为10s，最小值是2s。注意单次定位请求前设置。
         注意: 单次定位超时时间从确定了定位权限(非kCLAuthorizationStatusNotDetermined状态)
         后开始计算。
         */
        manager.locationTimeout = 10
        return manager
    }()
    
}

extension MapViewController {
    
    func hc_createMapView() {
        mapView = BMKMapView.init(frame: self.view.bounds)
        mapView.delegate = self
        
        //打开实时路况图层
        mapView.isTrafficEnabled = false
        mapView.userTrackingMode = BMKUserTrackingModeFollow
        
        //开启定位服务
        locationManager.startUpdatingHeading()
        locationManager.locatingWithReGeocode = true
        locationManager.startUpdatingLocation()

        //显示定位图层
        mapView.showsUserLocation = true
        self.view.addSubview(mapView)
        
        hc_addAnnotation()
    }
    
    func hc_addAnnotation() {
        
        startAnnotation = BMKPointAnnotation.init()
        startAnnotation?.coordinate = CLLocationCoordinate2D(latitude: 31.836246, longitude: 117.309512)
        startAnnotation?.title = "出发位置"
        
        endAnnotation = BMKPointAnnotation.init()
        endAnnotation?.coordinate = CLLocationCoordinate2D(latitude: 31.761387, longitude: 117.195037)
        endAnnotation?.title = "到达位置"
        
        let annotations = [startAnnotation, endAnnotation]
        mapView.addAnnotations(annotations as [Any])
        
    }
    
    func setupDefaultData() {
        
        //初始化BMKRouteSearch实例
        let drivingRouteSearch = BMKRouteSearch()
        //设置驾车路径的规划
        drivingRouteSearch.delegate = self
        /*
         线路检索节点信息类，一个路线检索节点可以通过经纬度坐标或城市名加地名确定
         实例化线路检索节点信息类对象
         */
        let start = BMKPlanNode()
        //起点坐标
        start.pt = CLLocationCoordinate2D.init(latitude: 31.836246, longitude: 117.309512)
        //起点名称
//        start.name = "绿地赢海国际大厦"
        //起点所在城市，注：cityName和cityID同时指定时，优先使用cityID
        start.cityName = "合肥市"
        
        //实例化线路检索节点信息类对象
        let end = BMKPlanNode()
        //终点坐标
        end.pt = CLLocationCoordinate2D.init(latitude: 31.761387, longitude:117.195037)
        //终点名称
//        end.name = "幸福大街"
        //终点所在城市，注：cityName和cityID同时指定时，优先使用cityID
        end.cityName = "合肥市"
        //初始化请求参数类BMKDrivingRoutePlanOption的实例
        let drivingRoutePlanOption = BMKDrivingRoutePlanOption()
        //检索的起点，可通过关键字、坐标两种方式指定。cityName和cityID同时指定时，优先使用cityID
        drivingRoutePlanOption.from = start
        //检索的终点，可通过关键字、坐标两种方式指定。cityName和cityID同时指定时，优先使用cityID
        drivingRoutePlanOption.to = end
        //途经点
//        drivingRoutePlanOption.wayPointsArray = option.wayPointsArray
        //驾车策略
        drivingRoutePlanOption.drivingPolicy = BMK_DRIVING_DIS_FIRST
        //路线中每一个step的路况
        drivingRoutePlanOption.drivingRequestTrafficType = BMK_DRIVING_REQUEST_TRAFFICE_TYPE_PATH_AND_TRAFFICE
        /**
         发起驾乘路线检索请求，异步函数，返回结果在BMKRouteSearchDelegate的onGetDrivingRouteResult中
         */
        let flag = drivingRouteSearch.drivingSearch(drivingRoutePlanOption)
        if flag {
            NSLog("驾车检索成功")
        } else {
            NSLog("驾车检索失败")
        }
        
    }
    
    //根据polyline设置地图范围
    func mapViewFitPolyline(_ polyline: BMKPolyline, _ mapView: BMKMapView) {
//        mapView.showsUserLocation = false
        
        var leftTop_x: Double = 0
        var leftTop_y: Double = 0
        var rightBottom_x: Double = 0
        var rightBottom_y: Double = 0
        if polyline.pointCount < 1 {
            return
        }
        let pt: BMKMapPoint = polyline.points[0]
        leftTop_x = pt.x
        leftTop_y = pt.y
        //左上方的点lefttop坐标（leftTop_x，leftTop_y）
        rightBottom_x = pt.x
        rightBottom_y = pt.y
        
        for index in 1..<polyline.pointCount {
            let point: BMKMapPoint = polyline.points[Int(index)]
            if (point.x < leftTop_x) {
                leftTop_x = point.x
            }
            if (point.x > rightBottom_x) {
                rightBottom_x = point.x
            }
            if (point.y < leftTop_y) {
                leftTop_y = point.y
            }
            if (point.y > rightBottom_y) {
                rightBottom_y = point.y
            }
        }
        var rect: BMKMapRect = BMKMapRect()
        rect.origin = BMKMapPointMake(leftTop_x, leftTop_y)
        rect.size = BMKMapSizeMake(rightBottom_x - leftTop_x, rightBottom_y - leftTop_y)
        let padding: UIEdgeInsets = UIEdgeInsets.init(top: 10, left: 50, bottom: 10, right: 50)
        mapView.fitVisibleMapRect(rect, edgePadding: padding, withAnimated: true)
    }
    
}

extension MapViewController: BMKMapViewDelegate {
    
    func mapView(_ mapView: BMKMapView!, viewFor annotation: BMKAnnotation!) -> BMKAnnotationView! {
        
        if annotation.isKind(of: BMKPointAnnotation.self) {
            if annotation.isEqual(startAnnotation) {
                var startAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "start")
                if startAnnotationView == nil {
                    startAnnotationView = BMKAnnotationView(annotation: annotation, reuseIdentifier: "start")
                }
                startAnnotationView?.image = UIImage(named: "icon_map_start")
                annotationView = startAnnotationView
            }
            
            if annotation.isEqual(endAnnotation) {
                var endAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "end")
                if endAnnotationView == nil {
                    endAnnotationView = BMKAnnotationView(annotation: annotation, reuseIdentifier: "end")
                }
                endAnnotationView?.image = UIImage(named: "icon_map_end")
                annotationView = endAnnotationView
            }
            
        }
        
        if annotation.isKind(of: BMKUserLocation.self) {
            let pointReuseIndetifier = "userLocationStyleReuseIndetifier"
            annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: pointReuseIndetifier)
            
            if annotationView == nil {
                annotationView = BMKAnnotationView(annotation: annotation, reuseIdentifier: pointReuseIndetifier)
            }
            annotationView?.image = UIImage.init(named: "icon_userlocation_image")
            self.customUserLocationView = annotationView
            
        }
        
        return annotationView
        
    }
    
    /**
     根据overlay生成对应的BMKOverlayView
     
     @param mapView 地图View
     @param overlay 指定的overlay
     @return 生成的覆盖物View
     */
    func mapView(_ mapView: BMKMapView!, viewFor overlay: BMKOverlay!) -> BMKOverlayView! {
        if overlay.isKind(of: BMKPolyline.self) {
            //初始化一个overlay并返回相应的BMKPolylineView的实例
            let polylineView = BMKPolylineView.init(overlay: overlay)
//            //设置polylineView的填充色
//            polylineView?.fillColor = UIColor.cyan.withAlphaComponent(1)
//            //设置polylineView的画笔（边框）颜色
//            polylineView?.strokeColor = UIColor.cyan.withAlphaComponent(0.7)
//            //设置polygonView的线宽度
            polylineView?.lineWidth = 14.0
            polylineView?.loadStrokeTextureImages([UIImage(named: "traffic_texture_blue")!, UIImage(named: "traffic_texture_green")!, UIImage(named: "traffic_texture_yellow")!, UIImage(named: "traffic_texture_red")!])
            
            return polylineView
        }
        return nil
    }
}

extension MapViewController: BMKLocationManagerDelegate {
    
    /**
     @brief 该方法为BMKLocationManager提供设备朝向的回调方法
     @param manager 提供该定位结果的BMKLocationManager类的实例
     @param heading 设备的朝向结果
     */
    func bmkLocationManager(_ manager: BMKLocationManager, didUpdate heading: CLHeading?) {
//        NSLog("用户方向更新")
        userLocation.heading = heading
        mapView.updateLocationData(userLocation)
    }
    
    /**
     @brief 连续定位回调函数
     @param manager 定位 BMKLocationManager 类
     @param location 定位结果，参考BMKLocation
     @param error 错误信息。
     */
    func bmkLocationManager(_ manager: BMKLocationManager, didUpdate location: BMKLocation?, orError error: Error?) {
        if let _ = error?.localizedDescription {
            NSLog("locError:%@;", (error?.localizedDescription)!)
            return
        }
        userLocation.location = location?.location
//        mapView.centerCoordinate = (location?.location!.coordinate)!
//        mapView.zoomLevel = 19
        
        print("\(location?.location?.coordinate.latitude ?? 0.0), \(location?.location?.coordinate.longitude ?? 0.0)")
        
        if location != nil {//得到定位信息，添加annotation
        
            if location?.location != nil {
                
            }
            
            if location?.rgcData != nil {
                
            }
            print("位置信息: \(location?.rgcData?.province ?? ""), \(location?.rgcData?.city ?? ""), \(location?.rgcData?.district ?? ""), \(location?.rgcData?.town ?? ""), \(location?.rgcData?.street ?? ""), \(location?.rgcData?.locationDescribe ?? "")")

            if location?.rgcData?.poiList != nil {
                for  poi in (location?.rgcData?.poiList)! {
                    print("周围的描述信息——地点名: \(poi.name ?? ""), 地址属性: \(poi.addr ?? ""), 位置信息的可信度: \(poi.relaiability), 位置标签: \(poi.tags ?? ""))")
                }
                
            }
            
            if location?.rgcData?.poiRegion != nil {
                print("地点名: \(location?.rgcData?.poiRegion.name ?? ""), 地点标签属性\(location?.rgcData?.poiRegion.tags ?? "")")

            }

        }
        
        //实现该方法，否则定位图标不出现
        mapView.updateLocationData(userLocation)
    }
    
    /**
     @brief 当定位发生错误时，会调用代理的此方法
     @param manager 定位 BMKLocationManager 类
     @param error 返回的错误，参考 CLError
     */
    func bmkLocationManager(_ manager: BMKLocationManager, didFailWithError error: Error?) {
        NSLog("定位失败")
    }
}

//MARK:BMKRouteSearchDelegate
extension MapViewController: BMKRouteSearchDelegate {
    
    /**
     返回驾车路线检索结果
     
     @param searcher 检索对象
     @param result 检索结果，类型为BMKDrivingRouteResult
     @param error 错误码 @see BMKSearchErrorCode
     */
    func onGetDrivingRouteResult(_ searcher: BMKRouteSearch!, result: BMKDrivingRouteResult!, errorCode error: BMKSearchErrorCode) {
        
//        mapView.removeOverlays(mapView.overlays)
//        mapView.removeAnnotations(mapView.annotations)
        
        //BMKSearchErrorCode错误码，BMK_SEARCH_NO_ERROR：检索结果正常返回
        if error == BMK_SEARCH_NO_ERROR {
            //+polylineWithPoints: count:坐标点的个数
            var pointCount = 0
            //获取所有驾车路线中第一条路线
            let routeline: BMKDrivingRouteLine = result.routes[0]
            
            print("距离:\(routeline.distance)")
            print("耗时(秒):\(routeline.duration.seconds)")
            print("耗时(分):\(routeline.duration.minutes)")
            print("耗时(时):\(routeline.duration.hours)")
            print("耗时(天):\(routeline.duration.dates)")

            //遍历驾车路线中的所有路段
            for (_, item) in routeline.steps.enumerated() {
                //获取驾车路线中的每条路段
                let step: BMKDrivingStep = item as! BMKDrivingStep
//                //初始化标注类BMKPointAnnotation的实例
//                let annotation = BMKPointAnnotation()
//                //设置标注的经纬度坐标为子路段的入口经纬度
//                annotation.coordinate = step.entrace.location
//                //设置标注的标题为子路段的说明
//                annotation.title = step.entraceInstruction
//                /**
//                 当前地图添加标注，需要实现BMKMapViewDelegate的-mapView:viewForAnnotation:方法
//                 来生成标注对应的View
//                 @param annotation 要添加的标注
//                 */
//                mapView.addAnnotation(annotation)
                //统计路段所经过的地理坐标集合内点的个数
                pointCount += Int(step.pointsCount)
            }
            
            //+polylineWithPoints: count:指定的直角坐标点数组
            var points = [BMKMapPoint](repeating: BMKMapPoint(x: 0, y: 0), count: pointCount)
            var count = 0
            //路段的路况信息
            var tarffics: Array<NSNumber> = []
            //遍历驾车路线中的所有路段
            for (_, item) in routeline.steps.enumerated() {
                //获取驾车路线中的每条路段
                let step: BMKDrivingStep = item as! BMKDrivingStep
                //遍历每条路段所经过的地理坐标集合点
                for index in 0..<Int(step.pointsCount) {
                    //将每条路段所经过的地理坐标点赋值给points
                    points[count].x = step.points[index].x
                    points[count].y = step.points[index].y
                    count += 1
                }
//                print("路况信息:\(step.traffics ?? [])")
                for item in step.traffics {
                    tarffics.append(item)
                }

            }
            print("路况信息:\(tarffics)")

            //根据指定直角坐标点生成一段折线
//            let polyline = BMKPolyline(points: &points, count: UInt(pointCount))
            let polyline = BMKPolyline(points: &points, count: UInt(pointCount), textureIndex: tarffics)
            /**
             向地图View添加Overlay，需要实现BMKMapViewDelegate的-mapView:viewForOverlay:方法
             来生成标注对应的View
             @param overlay 要添加的overlay
             */
            mapView.add(polyline)
            //根据polyline设置地图范围
            mapViewFitPolyline(polyline!, mapView)
        }
    }
    
}
