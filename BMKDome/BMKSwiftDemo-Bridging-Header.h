//
//  BMKSwiftDemo-Bridging-Header.h
//  BMKDome
//
//  Created by Hanchao on 2019/10/17.
//  Copyright © 2019 hmcx. All rights reserved.
//

#ifndef BMKSwiftDemo_Bridging_Header_h
#define BMKSwiftDemo_Bridging_Header_h


#import <BaiduMapAPI_Base/BMKBaseComponent.h>//引入base相关所有的头文件
#import <BaiduMapAPI_Map/BMKMapComponent.h>//引入地图功能所有的头文件
#import <BaiduMapAPI_Search/BMKSearchComponent.h>//引入检索功能所有的头文件
#import <BaiduMapAPI_Cloud/BMKCloudSearchComponent.h>//引入云检索功能所有的头文件
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>//引入计算工具所有的头文件
#import <BaiduMapAPI_Map/BMKMapView.h>//只引入所需的单个头文件
#import <BMKLocationkit/BMKLocationComponent.h>
#import "BNaviService.h"        //导航
#import "ToolObject.h"

#endif /* BMKSwiftDemo_Bridging_Header_h */
