//
//  ViewController.swift
//  BMKDome
//
//  Created by Hanchao on 2019/10/17.
//  Copyright © 2019 hmcx. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var tableView       : UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "百度地图"
        self.navigationController?.navigationBar.isTranslucent = false
        
        tableView = UITableView.init(frame: self.view.bounds, style: .grouped)
        tableView?.dataSource = self
        tableView?.delegate = self
        self.view.addSubview(tableView!)
        
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        
                
    }


}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        
        let titleArray = ["地图", "导航", "猎鹰"]
        
        cell.textLabel?.text = titleArray[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.navigationController?.pushViewController(MapViewController(), animated: true)
            
        } else if indexPath.row == 1 {
            self.navigationController?.pushViewController(BMKNavViewController(), animated: true)
        }
    }
    
}
