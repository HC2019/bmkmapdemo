//
//  ToolObject.m
//  BMKDome
//
//  Created by Hanchao on 2019/10/24.
//  Copyright © 2019 hmcx. All rights reserved.
//

#import "ToolObject.h"
#import "BNaviService.h"        //导航

@implementation ToolObject

+(void)setDisableOpenUrl:(BOOL)disable {
    [[[BNaviService getInstance] routePlanManager] setDisableOpenUrl:disable];
}

@end
