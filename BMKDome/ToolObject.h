//
//  ToolObject.h
//  BMKDome
//
//  Created by Hanchao on 2019/10/24.
//  Copyright © 2019 hmcx. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ToolObject : NSObject

+ (void) setDisableOpenUrl:(BOOL) disable;

@end

NS_ASSUME_NONNULL_END
