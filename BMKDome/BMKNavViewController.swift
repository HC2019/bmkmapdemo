//
//  BMKNavViewController.swift
//  BMKDome
//
//  Created by Hanchao on 2019/10/21.
//  Copyright © 2019 hmcx. All rights reserved.
//

import UIKit

class BMKNavViewController: UIViewController {

    deinit {
        print("释放了")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "地图导航"
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            if self.checkServicesInited() {
                self.startNav()
            }
        }
    }
    
    func checkServicesInited() -> Bool {
        if !(BNaviService.getInstance()?.isServicesInited())! {
            print("引擎尚未初始化完成，请稍后再试")
            return false
            
        }
        return true
    }    

}


extension BMKNavViewController {
    
    func startNav() {
        //起点
        let startNode = BNRoutePlanNode.init()
        startNode.pos = BNPosition.init()
        startNode.pos.x = 117.309512
        startNode.pos.y = 31.836246
        startNode.pos.eType = BNCoordinate_BaiduMapSDK
        
        //终点
        let endNode = BNRoutePlanNode.init()
        endNode.pos = BNPosition.init()
        endNode.pos.x = 117.195037
        endNode.pos.y = 31.761387
        endNode.pos.eType = BNCoordinate_BaiduMapSDK
        
        let nodesArray = [startNode, endNode]
        
        ToolObject.setDisableOpenUrl(true)
        BNaviService.getInstance()?.routePlanManager()?.startNaviRoutePlan(BNRoutePlanMode_Recommend, naviNodes: nodesArray, time: nil, delegete: self, userInfo: nil)
             
    }
    
}

extension BMKNavViewController: BNNaviRoutePlanDelegate {
    
    func routePlanDidFinished(_ userInfo: [AnyHashable : Any]!) {
        print("算路成功")
        BNaviService.getInstance()?.uiManager()?.showPage(BNaviUI_NormalNavi, delegate: self, extParams:nil)
//        BNaviService.getInstance()?.uiManager()?.showPage(BNaviUI_NormalNavi, delegate: self, extParams: [BNaviUI_NormalNavi_TypeKey: BN_NaviTypeSimulator])
        
    }
    
    func routePlanDidFailedWithError(_ error: Error!, andUserInfo userInfo: [AnyHashable : Any]! = [:]) {
        print("算路失败回调数据:\(String(describing: error)), \(userInfo ?? [:])")
        
    }
    
    func searchDidFinished(_ userInfo: [AnyHashable : Any]!) {
        print("检索成功回调数据:\(userInfo ?? [:])")
        
    }
    
    func updateRoadConditionDidFinished(_ pbData: Data!) {
        print("更新路况成功回调数据:\(pbData ?? Data())")

        
    }

}

extension BMKNavViewController: BNNaviUIManagerDelegate {
    
    func onExitPage(_ pageType: BNaviUIType, extraInfo: [AnyHashable : Any]!) {
        
        
    }
}
